﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UDPchat
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        private User _user;
        private Window _loginWindow;
        private Thread _listener;


        private enum KEY_WORDS
        {
            NONE,
            NICK,
            MSG
        }

        public ChatWindow(User user, Window loginWindow)
        {
            InitializeComponent();

            _user = user;
            _loginWindow = loginWindow;

            _listener = new Thread(StartListening);
            _listener.Start();
        }

        private void StartListening()
        {
            try
            {
                while (true)
                {
                    String received = UdpService.Receive();
                    KEY_WORDS keyWord = ChackIfContainsKeyWord(received);

                    switch (keyWord)
                    {
                        case KEY_WORDS.MSG:
                            Message(received);
                            break;

                        case KEY_WORDS.NICK:
                            Nick(received);
                            break;
                    }
                    
                }
            }
            catch(ThreadAbortException e)
            {
                Console.WriteLine("listener aborded");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        
        private KEY_WORDS ChackIfContainsKeyWord(String str)
        {
            
            if (str.StartsWith("NICK"))
            {
                return KEY_WORDS.NICK;
            }
            else if(str.StartsWith("MSG"))
            {
                return KEY_WORDS.MSG;
            }

            return KEY_WORDS.NONE;
        }

        private bool IsNickTaken(String str)
        {
            if (!str.EndsWith("BUSY"))
            {
                String[] tempStr = str.Split(' ');
                if (_user.Nick.Equals(tempStr[1]))
                {
                    return true;
                }
            }

            return false;
        }

        private void OnSendClick(object sender, RoutedEventArgs e)
        {
            if (!textBoxMessage.Text.Equals(""))
            {
                StringBuilder preperMSG = new StringBuilder();
                preperMSG.Append("MSG").Append(" ")
                    .Append(_user.Nick).Append(" ")
                    .Append(textBoxMessage.Text);
                UdpService.Send(preperMSG.ToString());
                textBoxMessage.Text = "";
            }
        }

        private void Nick(string received)
        {
            if (IsNickTaken(received))
            {
                StringBuilder preperMsgToSend = new StringBuilder();
                preperMsgToSend.Append(received).Append(" BUSY");
                UdpService.Send(preperMsgToSend.ToString());
            }
        }

        private void Message(string received)
        {
            int indexOfSpace = received.IndexOf(' ');
            String tempForStringWithoutKeyWord = received.Substring(indexOfSpace + 1);
            indexOfSpace = received.IndexOf(' ');
            String nick = tempForStringWithoutKeyWord.Substring(0, tempForStringWithoutKeyWord.IndexOf(' ') + 1);
            String message = tempForStringWithoutKeyWord.Substring(tempForStringWithoutKeyWord.IndexOf(' '));
            textBlockMessage.Dispatcher.Invoke(() =>
            {
                StringBuilder prepareMSG = new StringBuilder();

                prepareMSG.Append(nick).Append(":")
                .Append(Environment.NewLine)
                .Append('\t')
                .Append(message)
                .Append(Environment.NewLine);

                textBlockMessage.AppendText(prepareMSG.ToString());
            });
        }

        protected override void OnClosed(EventArgs e)
        {
            _loginWindow.Show();
            base.OnClosed(e);
        }
    }
}
