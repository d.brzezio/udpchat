﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPchat
{
    public class User
    {
        private string _nick;
        private string _room;
        
        public User(string nick, string room)
        {
            _nick = nick;
            _room = room;
        }
        
        public string Nick
        {
            get
            {
                return _nick;
            }
        }

        public string Room
        {
            get
            {
                return _room;
            }
        }
    }
}
