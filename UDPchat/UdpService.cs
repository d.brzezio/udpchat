﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UDPchat
{
    class UdpService
    {
        private static IPAddress _multicastAdress = IPAddress.Parse("224.0.0.1");

        private static IPEndPoint _endPointForSender = new IPEndPoint(_multicastAdress, 11111);
        private static IPEndPoint _endPointForListener = new IPEndPoint(IPAddress.Any, 11111);

        private static UdpClient _sender;
        private static UdpClient _listener;

        private static String _lastSendMessage;

        private volatile static int _timeout;

        private UdpService() { }


        public static int Timeout
        {
            get
            {
                return _timeout;
            }
            set
            {
                _timeout = value;
            }
        }

        public static UdpClient Sender
        {
            get
            {
                if(_sender == null)
                {
                    InitSender();
                }

                return _sender;
            }
        }

        private static void InitSender()
        {
            _sender = new UdpClient();
            _sender.JoinMulticastGroup(_multicastAdress);
        }

        public static UdpClient Listener
        {
            get
            {
                if(_listener == null)
                {
                    InitListener();
                }

                return _listener;
            }
        }

        private static void InitListener()
        {
            _listener = new UdpClient();
            _listener.ExclusiveAddressUse = false;
            _listener.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _listener.JoinMulticastGroup(_multicastAdress);
            _listener.Client.Bind(_endPointForListener);
        }

        public static void Send(String msg)
        {
            if(_sender == null)
            {
                InitSender();
            }

            _sender.Send(Encoding.ASCII.GetBytes(msg), 
                Encoding.ASCII.GetBytes(msg).Length, 
                _endPointForSender);

            _lastSendMessage = msg;
        }

        public static String Receive()
        {
            if(_listener == null)
            {
                InitListener();
            }

            _listener.Client.ReceiveTimeout = 0;
            Thread.Sleep(1000);

            byte[] bytes = _listener.Receive(ref _endPointForListener);

            return Encoding.ASCII.GetString(bytes);
        }

        public static String ReceiveWithTimeout(int timeout, String name)
        {
            if (_listener == null)
            {
                InitListener();
            }

            _timeout = timeout;

            try
            {
                String msg;
                byte[] bytes;
                do
                {
                    do
                    {
                        _listener.Client.ReceiveTimeout = _timeout;
                        if (_timeout == 0)
                            throw new SocketException();
                        bytes = _listener.Receive(ref _endPointForListener);
                        msg = Encoding.ASCII.GetString(bytes);
                        Console.WriteLine("Answer for nick:" + msg);
                    } while (!msg.Contains("BUSY"));
                } while (!msg.Contains(name));

                return Encoding.ASCII.GetString(bytes);
            }
            catch (SocketException e)
            {
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static void InitSockets()
        {
            InitListener();
            InitSender();
        }

        public static void CloseSockets()
        {
            if(_sender != null)
            {
                _sender.Close();
            }
            if(_listener != null)
            {
                _listener.Close();
            }

            _sender = null;
            _listener = null;
        }
    }
}
