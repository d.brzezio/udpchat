﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UDPchat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Thread _updater;
        private Task<String> _result;


        public MainWindow()
        {
            InitializeComponent();
            UdpService.InitSockets();
        }

        private async void BtnOnClick(object sender, RoutedEventArgs e)
        {
            if (!textBoxNick.Text.Equals(""))
            {
                btnConnect.IsEnabled = false;

                StringBuilder prepareMsgToSend = new StringBuilder();
                prepareMsgToSend.Append("NICK ").Append(textBoxNick.Text);
                UdpService.Send(prepareMsgToSend.ToString());

                _result = Task.Run(StartListening);

                _updater = new Thread(ProgressUpdate);
                _updater.Start();


                if (await _result == null)
                {
                    Console.WriteLine("Done");
                    Console.WriteLine(DateTime.Now);
                    User user = new User(textBoxNick.Text, "A");
                    ChatWindow chat = new ChatWindow(user, this);
                    chat.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Nick jest już zajęty");
                }

                _updater.Abort();
                _updater = null;
                progresBar.Value = 0;
                btnConnect.IsEnabled = true;
            }
        }

        private async Task<String> StartListening()
        {
            return UdpService
                .ReceiveWithTimeout(10000, textBoxNick.Dispatcher.Invoke( () => textBoxNick.Text));
        }

        private void ProgressUpdate()
        {
            progresBar.Dispatcher.Invoke(() => progresBar.Value = 0);
            do
            {
                progresBar.Dispatcher.Invoke(() => progresBar.Value += 10);
                Thread.Sleep(1000);
                UdpService.Timeout = UdpService.Timeout - 1000;
            } while (progresBar.Dispatcher.Invoke(() => progresBar.Value) < 100);

            
        }


        protected override void OnClosed(EventArgs e)
        {
            System.Console.WriteLine("Exit");
            UdpService.CloseSockets();

            if(_updater != null)
            {
                _updater.Abort();
            }
            base.OnClosed(e);
        }
    }
}
